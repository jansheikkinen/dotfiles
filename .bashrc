#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export XDG_CACHE_DIR="$HOME/.cache"
export EDITOR=nvim


ESC_BOLD="\[\033[1m\]"
ESC_NONE="\[\033[0m\]"

ESC_BLUE="\[\033[34m\]"
ESC_WHITE="\[\033[39m\]"
ESC_GREEN="\[\033[32m\]"
ESC_YELLOW="\[\033[33m\]"

PS1="$ESC_BOLD[$ESC_BLUE\u$ESC_WHITE@$ESC_GREEN\H $ESC_YELLOW\w$ESC_WHITE]\n$ $ESC_NONE"
PATH="${PATH}:$HOME/.local/bin"
