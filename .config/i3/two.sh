#!/bin/sh
sleep 0.5
xrandr --output LVDS1 --primary --mode 1366x768 --pos 0x156 \
  --output HDMI2 --mode 1920x1080 --pos 1366x0
nitrogen --restore
sleep 0.5
