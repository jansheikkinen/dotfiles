-- init.lua --

-- luacheck: globals vim

-- Options --
local options = {
  -- Search options
  showmatch = true,
  smartcase = true,
  hlsearch = true,
  incsearch = true,

  -- Tabs
  tabstop = 4,
  softtabstop = 4,
  shiftwidth = 4,
  expandtab = true,
  autoindent = true,
  breakindent = true,

  -- Mouse and clipboard
  mouse = "a",
  clipboard = "unnamedplus",

  -- Lines and columns
  relativenumber = true,
  number = true,
  cc = "81",
  wrap = true,
  cursorline = true,
  cursorcolumn = false,
  statuscolumn = "%=%{v:relnum?v:relnum:v:lnum} ",

  -- Split windows
  splitbelow = true,
  splitright = true,

  -- Folds
  foldlevelstart = 999,
}

local function set_options(opt)
  for option, value in pairs(opt) do
    vim.opt[option] = value
  end

  vim.g.mapleader = ","
  vim.g.rust_recommended_style = true
  vim.g.markdown_recommended_style = false
  vim.g.zig_recommended_style = false
  vim.g.zig_fmt_autosave = false

  vim.cmd("colorscheme theme")

  vim.cmd("filetype plugin on")
  vim.cmd("filetype plugin indent on")
  vim.cmd("syntax on")

  -- These remove the diagnostics near the line numbers
  vim.fn.sign_define("DiagnosticSignError", {
    text = nil, texthl = "DiagnosticSignError",
    linehl = nil, numhl = "DiagnosticLineNrError" })

  vim.fn.sign_define("DiagnosticSignWarn", {
    text = nil, texthl = "DiagnosticSignWarn",
    linehl = nil, numhl = "DiagnosticLineNrWarn" })

  vim.fn.sign_define("DiagnosticSignInfo", {
    text = nil, texthl = "DiagnosticSignInfo",
    linehl = nil, numhl = "DiagnosticLineNrInfo" })

    vim.fn.sign_define("DiagnosticSignHint", {
    text = nil, texthl = "DiagnosticSignHint",
    linehl = nil, numhl = "DiagnosticLineNrHint" })
end


-- Plugins --

-- Download packer if not already installed
local function bootstrap_packer()
  local install_path = vim.fn.stdpath("data").."/site/pack/packer/start/packer.nvim"

  if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
    vim.fn.system({
      "git", "clone", "--depth", "1",
      "https://github.com/wbthomason/packer.nvim", install_path
    })
    vim.cmd("packadd packer.nvim")
    return true
  end
  return false
end

-- List of plugins for packer to manage, including itself
local function load_plugins()
  local bootstrapped = bootstrap_packer()

  return require("packer").startup(function(use)
    use("wbthomason/packer.nvim") -- the package manager

    use("neovim/nvim-lspconfig") -- neovim"s builtin LSP
    use("p00f/clangd_extensions.nvim") -- clangd"s extra LSP utilities
    -- use("ziglang/zig.vim") -- zig LSP stuff

    -- syntax highlighting
    use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" })
    use("nvim-treesitter/nvim-treesitter-context")
    use("stefanos82/nelua.vim") -- nelua highlighting
    use("Mfussenegger/nvim-lint") -- linter

    use("lukas-reineke/indent-blankline.nvim") -- indentation marker
    use("jghauser/mkdir.nvim") -- create missing dirs when saving file
    use("mcauley-penney/tidy.nvim") -- remove trailing whitespace
    use("andweeb/presence.nvim") -- allow discord to stalk my neovim usage
    use("akinsho/toggleterm.nvim") -- toggle-able terminal
    use("numToStr/Comment.nvim") -- commenting for cool kids
    use("https://git.sr.ht/~whynothugo/lsp_lines.nvim") -- diagnostic formatting
    use("rmagatti/auto-session") -- sessions
    use("windwp/nvim-autopairs") -- autopairs

-- use("hrsh7th/nvim-cmp") -- autocompletion engine
    -- use("hrsh7th/cmp-nvim-lsp")
    -- use("hrsh7th/cmp-buffer")
    -- use("hrsh7th/cmp-path")
    -- use("hrsh7th/cmp-cmdline")
    -- use("hrsh7th/cmp-nvim-lsp-signature-help")
    --
    -- use("L3MON4D3/LuaSnip") -- snippets
    -- use("saadparwaiz1/cmp_luasnip")
    -- use("rafamadriz/friendly-snippets")

    -- install packer if not already installed
    if bootstrapped then
      require("packer").sync()
    end
  end)
end


local plugin_configs = { }

-- Semantic highlighting for the languages that I use most
function plugin_configs.treesitter()
  require("nvim-treesitter.configs").setup({
    ensure_installed = {
      "c", "cpp", "lua", "python", "zig", "rust",
      "scheme", "json", "java", "typescript",
      "haskell", "racket", "javascript", "ebnf"
    },
    highlight = {
      enable = true,
      additional_vim_regex_highlighting = true,
    },
    incremental_selection = { enable = true, },
    textobjects = { enable = true, },
  })

  require("treesitter-context").setup({
    max_lines = 3,
    mode = "topline",
  })
end


-- Set up linters
function plugin_configs.linter()
  require("lint").linters_by_ft = {
    python = { "mypy" },
    c = { "clangtidy" },
  }

  vim.api.nvim_create_autocmd({ "InsertLeave", "BufReadPost" },
  {
    callback = function()
      require("lint").try_lint()
    end
  })

  vim.diagnostic.config({ virtual_text = false })

  local opts = {
    noremap = true,
    silent = true,
  }

  vim.keymap.set("n", "<Space>e", vim.diagnostic.open_float, opts)
  vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
  vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
  vim.keymap.set("n", "<Space>q", vim.diagnostic.setloclist, opts)
end


-- Marks levels of indentation, as well as whitespace and EOL
function plugin_configs.indent()
  vim.opt.list = true
  vim.opt.listchars:append "eol:↴"

  require("ibl").setup({
    scope = {
      show_start = false,
      show_end = false,
    },
    whitespace = {
    }
  })
end


-- Autocomplete and snippets
-- function plugin_configs.completion()
--   local cmp = require("cmp")
--   local luasnip = require("luasnip")
--   require("luasnip.loaders.from_vscode").lazy_load()
--
--   cmp.setup({
--     snippet = {
--       expand = function(args)
--         require("luasnip").lsp_expand(args.body)
--       end,
--     },
--
--     mapping = {
--       ["<C-a>"] = cmp.mapping.confirm({ select = false }),
--       ["<C-e>"] = cmp.mapping.abort(),
--
--       ["<C-k>"] = cmp.mapping(function(fallback)
--         if luasnip.expand_or_jumpable() then
--           luasnip.expand_or_jump()
--         else fallback() end
--       end, { "i", "s" }),
--       ["<C-j>"] = cmp.mapping(function(fallback)
--         if luasnip.jumpable(-1) then
--           luasnip.jump(-1)
--         else fallback() end
--       end, { "i", "s" }),
--
--       ["<C-h>"] = cmp.mapping(function(fallback)
--         if cmp.visible() then
--           cmp.select_next_item()
--         else fallback() end
--       end, { "i", "s" }),
--       ["<C-l>"] = cmp.mapping(function(fallback)
--         if cmp.visible() then
--           cmp.select_prev_item()
--         else fallback() end
--       end, { "i", "s" }),
--     },
--
--     sources = {
--       { name = "nvim_lsp" },
--       { name = "luasnip" },
--       { name = "nvim_lsp_signature_help" },
--       { name = "buffer" },
--       { name = "path" },
--       { name = "cmdline" },
--     },
--     completion = {
--       keyword_length = 3,
--     },
--     experimental = {
--       ghost_text = true
--     }
--   })
-- end


-- Language servers my beloved
function plugin_configs.lsp()
  local lsp = require("lspconfig")

  -- Hook up autocompletion to the LSP
  --local capabilities = require("cmp_nvim_lsp").default_capabilities()

  -- stuff to do whenever an LSP is started
  local function on_attach(client, bufnr)
    vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

    -- Keymappings
    local opts = {
      noremap = true,
      silent = true,
      buffer = bufnr
    }

    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
    vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)
    vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, opts)
    vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
    vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, opts)
    vim.keymap.set("n", "<space>f", vim.lsp.buf.format, opts)

    vim.keymap.set("n", "ch", "<Cmd>ClangdSwitchSourceHeader<CR>")
    vim.keymap.set("n", "cn", "<Cmd>ClangAST<CR>")
  end

  -- Rust LSP
  lsp.rust_analyzer.setup({
    capabilities = capabilities,
    on_attach = on_attach
  })

  -- Zig LSP
  lsp.zls.setup({
    capabilities = capabilities,
    on_attach = on_attach,
  })

  -- Typescript LSP
  lsp.tsserver.setup({
    capabilities = capabilities,
    on_attach = on_attach,
  })

  -- Haskell LSP
  lsp.hls.setup({
    filetypes = { "hs" },
    capabilities = capabilities,
    on_attach = on_attach,
  })

  lsp.racket_langserver.setup({
    capabilities = capabilities,
    on_attach = on_attach,
  })

  -- Lua LSP
  lsp.lua_ls.setup({
    capabilities = capabilities,
    on_attach = on_attach,
    settings = {
      Lua = {
        diagnostics = {
          globals = {
            "vim",
          },
        },
        workspace = {
          library = vim.api.nvim_get_runtime_file("", true),
          checkThirdParty = false,
        },
        telemetry = {
          enabled = false,
        },
      },
    },
  })

  lsp.clangd.setup({
    filetypes = { "c", "h", "cpp", "hpp", "objcpp", "cuda", "proto", },
    capabilities = capabilities,
    on_attach = on_attach
  })

  -- -- Clangd LSP
  -- -- This still uses neovim"s lsp, just goes through another plugin first
  -- require("clangd_extensions").setup({
  --   server = {
  --     capabilities = capabilities,
  --     cmd = { "clangd", "--background-index", "--clang-tidy" },
  --     on_attach = on_attach,
  --   },
  --   extensions = {
  --     autosetHints = true,
  --     inlay_hints = {
  --       only_current_line = false,
  --       only_current_line_autocmd = "CursorHold",
  --       show_parameter_hints = true,
  --       parameter_hints_prefix = " <- ",
  --       other_hints_prefix = " => ",
  --       max_len_align = true,
  --       max_len_align_padding = 1,
  --       right_align = false,
  --       right_align_padding = 7,
  --       highlight = "Pmenu",
  --       priority = 100,
  --     },
  --   },
  -- })
end


-- Let discord see which files I'm editing for cool bragging rights B)
function plugin_configs.presence()
  require("presence"):setup({
    buttons = {
      { label = "hub of git", url = "https://github.com/jansheikkinen" },
    },
  })
end


-- A toggleable terminal window, since I only need it infrequently
function plugin_configs.toggleterm()
  require("toggleterm").setup({
    open_mapping = "<C-\\>",
    direction = "float",
  })
end


-- Any plugins that use the default configuration
function plugin_configs.default()
  require("tidy").setup()
  require("Comment").setup()
  require("lsp_lines").setup()
  require("auto-session").setup({})
  require("nvim-autopairs").setup()
end


-- Just some keybinds for plugin management
function plugin_configs.packer()
  vim.keymap.set("n", "<leader>ps", "<cmd>PackerSync<cr>")
  vim.keymap.set("n", "<leader>so", "<cmd>so $MYVIMRC<cr>")
end


-- Load the configurations for each plugin
local function configure_plugins()
  for _, fn in pairs(plugin_configs) do
    fn()
  end
end


-- Main --

local function main()
  set_options(options)
  load_plugins()
  configure_plugins()
end

main()
