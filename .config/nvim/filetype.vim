" JHScript Filetype

if exists("did_load_filetype")
  finish
endif

au BufRead,BufNewFile *.jhs set filetype=jhscript
