" Vim syntax file
" Language: JHScript
" Maintainer: Jans Heikkinen

" Usage: Simply edit the path in the update-syntax-vim script and run it to
" automatically place relevant language files in that path. Run the script
" again any time one of the files is changed. Note that the script will ask you
" before overwriting any existing files, so please be sure that it isn't
" overwriting anything important before you confirm it

if exists("b:current_syntax")
  finish
endif

syn case match

syn match jhscriptConstant "[A-Z_][A-Z0-9_]*"
syn match jhscriptIdentifier "[a-zA-Z_][a-zA-Z0-9_]*"

syn region jhscriptString start="\"" end="\""
syn region jhscriptChar   start="\'" end="\'"

syn match jhscriptNumber "\<\d\+\>"
syn match jhscriptNumber "\<\d\+r-\?[0-9A-Z]\+\>"
syn match jhscriptNumber "\<\d\+\.\d*\>"

syn match   jhscriptOperator "[!<>=~^&|*/%+-]\|\.\{2,3}"
syn keyword jhscriptConstant nil undefined true false

syn keyword jhscriptType any int float string bool void type char

syn keyword jhscriptStructure struct union
syn keyword jhscriptStatement let const struct function
      \ if else while return break continue import print as

syn keyword jhscriptTodo contained TODO FIXME NOTE
syn match   jhscriptComment "//.*$" contains=jhscriptTodo

hi def link jhscriptConstant   Constant
hi def link jhscriptIdentifier Identifier
hi def link jhscriptString     String
hi def link jhscriptChar       String
hi def link jhscriptNumber     Number
hi def link jhscriptOperator   Operator
hi def link jhscriptConstant   Constant
hi def link jhscriptType       Type
hi def link jhscriptStructure  Structure
hi def link jhscriptStatement  Statement
hi def link jhscriptTodo       Todo
hi def link jhscriptComment    Comment

let b:current_syntax = "jhscript"
